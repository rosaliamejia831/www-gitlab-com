---
layout: markdown_page
title: "Category Direction - Runner SaaS"
description: "Hosted GitLab Runners available on GitLab SaaS and that support the GitLab Build Cloud for Linux, Windows and macOS."
canonical_path: "/direction/verify/runner_saas/"
---

- TOC
  {:toc}

## Runner SaaS Overview

Runner SaaS is the product development effort for Runners on GitLab SaaS.

## Vision

Our vision for GitLab Runner SaaS is to provide developers with a zero-friction, secure, highly performant, and reliable CI/CD build experience for the market-leading operating systems and development technologies.

## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)

##  Runner SaaS Solutions

| Offer Name                                                                                 | Offer Status|
|--------------------------------------------------------------------------------------------|----------------|
| [Runner SaaS for Linux](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html) |GA|
| [Runner SaaS for Windows](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)                                                                |Beta|
| [Runner SaaS for macOS](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)                                                                  |Beta|

## Strategic Priorities

The table below represents the current strategic priorities for Runner SaaS. This list will change with each monthly revision of this direction page.

|Theme-Category| Item                                                                                                                 |Why?| Target delivery QTR |
|----------|----------------------------------------------------------------------------------------------------------------------|----------------|---------------------|
|macOS| [Runner SaaS for macOS Limited Availability](https://gitlab.com/gitlab-org/gitlab/-/issues/342848)                      |In Q3 FY22, we launched the Runner SaaS for macOS beta. To date, the user and customer demand for this service has been exceptional. And further reinforcing the one DevOps strategy, the current beta participants strongly favor having a fully integrated build solution for the Apple ecosystem embedded in GitLab. We plan to launch this offer in GA and to evolve it so that customers on GitLab SaaS can simply focus on writing code  and be rest assured that the capabilities to build their software is fully managed by GitLab| FY23 Q2             |
|SaaS Runners on Linux|[Experiment - offer GCP n1-standard-2 VM's on GitLab SaaS Linux Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/357676); [Experiment - offer GCP n1-standard-4 VM's on GitLab SaaS Linux Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/357681)|The ongoing feedback from customers and users is that the current build machine type offered on the SaaS Runners on Linux is not powerful enough to run specific workflows efficiently. In one example, a similar CI job on a competing hosted solution ran in 12 minutes compared to 28 minutes on the GitLab SaaS Runners. To accelerate offering premium machine types on GitLab SaaS, an initial MVC will be to experiment with offering two new classes of build machines on the Saas Runners on Linux. This effort will inform the work planned in Q4 to extend the various compute offerings available on the SaaS Runners on Linux. |FY23 Q2|
|Scalability| [Implement a new autoscaling solution - Runner SaaS for Linux OS](https://gitlab.com/groups/gitlab-org/-/epics/6995) |The Runner SaaS for Linux handles millions of CI jobs each month. The autoscaling architecture that is the foundation of this solution has served us well. However, since its introduction, we have launched new autoscaling solutions to manage the Runner SaaS for Windows and macOS. In addition, the core technology used for Linux, Docker Machine, is no longer maintained by Docker. Though we maintain a fork of Docker Machine, the right long-term strategy is to iterate towards the next generation autoscaler for the Runner SaaS. This architectural technology pivot will enable us to accomplish several goals. First - enabling GitLab to scale over the next five years. And in the near term, ensure that we can consistently achieve and exceed our CI Runners SLO goal of 99.95% availability.| FY23 Q3             |
|Scalability/Foundational Requirement| [Segmentation by plan type](https://gitlab.com/gitlab-org/gitlab/-/issues/323525#)                                   |Today we do not delineate access to tiers of service on Runner SaaS by plan type. As we continue to grow GitLab SaaS, a critical next phase is to offer different Runner SaaS capabilities and service levels by plan type. This foundational capability not only sets the stage for those future offers, but is also another pillar in enabling us to consistently deliver service levels in line with our stated SLO targets.| FY23 Q3             |
|Windows| [Runner SaaS for Windows General Availability](https://gitlab.com/gitlab-org/gitlab/-/issues/300476)                    |We launched the Runner SaaS for Windows in beta in FY21Q1. At the time, our initial plan was to transition the offer to GA within a few quarters. However, due to other strategic priorities, we delayed that timeline. Now users and customers rely on a Windows build solution hosted on GitLab SaaS for their mission-critical CI/CD builds, so we must migrate the Windows offer to GA.| FY23 Q3             |
|Linux OS + Docker Runners| [Offer multiple compute options on GitLab SaaS](https://gitlab.com/groups/gitlab-org/-/epics/2426)                   |There has been increasing demand to offer more compute offerings on GitLab SaaS Runners from customer feedback in the past few months. For some customers, higher compute GitLab SaaS Runners is a must-have for migrating more CI/CD workloads to GitLab SaaS. The launch of this offer will have a net positive impact on the Verify stage monthly active users. However, there are foundational elements that we need to implement on GitLab SaaS to provide the user and customer community a first-class experience.| FY23 Q4             |

## Ongoing Maintenance

In conjunction with the development work required to deliver the strategic priorities listed above, in each milestone, the Runner SaaS team will devote up to 40% of available developer capacity across the categories listed below.

- macOS build VM image updates and maintenance.
- Windows build VM image updates and maintenance.
- Bugs related to executing builds on Runner SaaS (Linux + Docker, macOS, Windows).
- Collaboration with the SRE team to resolve incidents related to Runner SaaS..
- Installing new runner version updates to the runner managers on Runner SaaS.

## Maturity Plan

The Runner SaaS maturity level is ["Minimal"](/direction/maturity/). We are also evaluating this category with a Category maturity scorecard via [gitlab&6090](https://gitlab.com/groups/gitlab-org/-/epics/6090). For more information (see our [definitions of maturity levels](/direction/maturity/)).

## Competitive Landscape

Organizations that use Cloud-native CI/CD solutions, such as GitLab.com, CircleCI, and GitHub, can run their CI/CD pipelines and get to a first green build without setting up build servers, installing and configuring build agents, or runners.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, multiple build server and configuration options, and on-demand scale.

# Competitive Matrix 

### Docker Container Builds - Hosted Build Machines

| Machine Type | GitLab | GitHub | CircleCI |
| ------ | :-----: |  :-----: |  :-----:|
| 1 vCPU, 4GB RAM class build VM | Available | Not available | Available |
| 2 vCPU, 8GB RAM class build VM | Not available | Available | Available |
| 4 vCPU, 16GB RAM class build VM | Not available | Not available | Available |
| 8 vCPU, 32GB RAM class build VM | Not available | Not available | Available |
| 16 vCPU, 32GB RAM class build VM | Not available | Not available | Available |

### Windows Builds - Hosted Build Machines

| Machine Type                   | GitLab        | GitHub        |
| ------------------------------ | ------------- | ------------- |
| 2 vCPU, 8GB RAM class build VM | Available     | Not available |
| 3 vCPU, 16GB RAM class build VM |Not available | Available     |


### macOS - Offer Positioning and Hosted Build Machines

|....||GitHub|Apple - Xcode Cloud (beta) |CircleCI|Bitrise.io|
|----------|----------------|----------------|----------------|----------------|----------------|
|Positioning Statement ||A GitHub-hosted runner is  VM hosted by GitHub with the GitHub actions runner application installed.|A CI/CD service built into Xcode, designed expressly for Apple developers.|Industry-leading speed. No other CI/CD platform takes performance as seriously as we do.|Build better mobile applications, faster|
|Value Proposition||When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of.|Build your apps in the cloud and eliminate dedicated build infrastructure.| The macOS execution environment allows you to test, build, and deploy macOS and iOS apps on CircleCI.|CI for mobile - save time spent on testing, onboarding, and maintenance with automated workflows and triggers|
|macOS Virtual Machine Specs||3-core CPU, 14 GB RAM |TBD|Medium: 4 vCPU, 8 GB RAM; Large: 8 vCPU, 16 GB RAM;  Metal 12 vCPU 32 GB RAM|Standard: 4 vCPU, 19 GB RAM; Elite 8 vCPU 35 GB ram; Elite XL 12 vCPU 54 GB RAM|


## Give Feedback

If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2022-04-04
