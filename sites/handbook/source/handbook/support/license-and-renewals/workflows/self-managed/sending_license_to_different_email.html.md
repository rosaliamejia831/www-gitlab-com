---
layout: markdown_page
title: Sending licenses to a different email address
description: "Information on the process for sending licenses to a different email address"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

## Overview

We currently do not send paid licenses out to any email other than the one used for purchasing (and within customers.gitlab.com). To have it sent to a different email address, we would need to follow the process outlined at [Associating purchases with additional accounts](/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases.html).

Once the account is changed, the license can be re-sent (or obtained by the new account owner).

** Note **

We cannot bypass doing this based on internal requests. It must be done via a customer submitted ticket.
