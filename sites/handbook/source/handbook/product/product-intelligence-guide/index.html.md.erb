---
layout: handbook-page-toc
title: Product Intelligence Guide
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Intelligence Overview

At GitLab, we collect product usage data for the purpose of helping us build a better product. Data helps GitLab understand which parts of the product need improvement and which features we should build next. Product usage data also helps our team better understand the reasons why people use GitLab. With this knowledge we are able to make better product decisions.

To get an introduction of Product Analytics at GitLab you have a look at the [GitLab Product Data Training](https://docs.google.com/presentation/d/1ySP9sndhF9BdRhaZhMK6kGbc8txO_UkAu48HmoxLtfI/edit#slide=id.g29a70c6c35_0_68) (internal) deck

There are several stages and teams involved to go from collecting data to making it useful for our internal teams and customers.

| Stage | Description | DRI | Support Teams |
| ----- | ----------- | --- | ------------- |
| Privacy Settings | The implementation of our Privacy Policy including data classification, data access, and user settings to control what data is shared with GitLab. | Product Intelligence | Legal, Data |
| Collection | The data collection tools used across all GitLab applications including GitLab SaaS, GitLab self-managed, CustomerDot, VersionDot, and [about.gitlab.com](https://about.gitlab.com/). Our current tooling includes Snowplow, Service Ping, and Google Analytics. | Product Intelligence | Infrastructure |
| Extraction | The data extraction tools used to extract data from Product, Infrastructure, Enterprise Apps data sources. Our current tooling includes Stitch, Fivetran, and Custom. | Data |  |
| Loading | The data loading tools used to extract data from Product, Infrastructure, Enterprise Apps data sources and to load them into our data warehouse. Our current tooling includes Stitch, Fivetran, and Custom. | Data |  |
| Orchestration | The orchestration of extraction and loading tooling to move data from sources into the Enterprise Data Warehouse. Our current tooling includes Airflow. | Data |  |
| Storage | The Enterprise Data Warehouse (EDW) which is the single source of truth for GitLab's corporate data, performance analytics, and enterprise-wide data such as Key Performance Indicators. Our current EDW is built on Snowflake. | Data |  |
| Transformation | The transformation and modelling of data in the Enterprise Data Warehouse in preparation for data analysis. Our current tooling is dbt and Python scripts. | Data | Product Intelligence |
| Analysis | The analysis of data in the Enterprsie Data Warehouse using a querying and visualization tool. Our current tooling is Sisense. | Data | Product Intelligence |

[Editable source file](https://docs.google.com/spreadsheets/d/144-BLh7uyX4aY23QNrvke5BqCcb9xfPk2BL4qGFvzFY/edit?usp=sharing)

## Systems Overview

The systems overview is a simplified diagram showing the interactions between GitLab Inc and self-managed instances.

![Product Intelligence Overview](product_intelligence_systems_overview.png)

[Source file](https://app.diagrams.net/#G13DVpN-XnhWGz9tqReIj8pp1UE4ehk_EC)

### GitLab Inc

For Product Intelligence purposes, GitLab Inc has three major components:

1. [Data Infrastructure](/handbook/business-ops/data-team/platform/infrastructure/): This contains everything managed by our data team including Sisense Dashboards for visualization, Snowflake for Data Warehousing, incoming data sources such as PostgreSQL Pipeline and S3 Bucket, and lastly our data collectors [GitLab.com's Snowplow Collector](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/library/snowplow/) and GitLab's Versions Application.
1. GitLab.com: This is the production GitLab application which is made up of a Client and Server. On the Client or browser side, a Snowplow JS Tracker (Frontend) is used to track client-side events. On the Server or application side, a Snowplow Ruby Tracker (Backend) is used to track server-side events. The server also contains Service Ping which leverages a PostgreSQL database and a Redis in-memory data store to report on usage data. Lastly, the server also contains System Logs which are generated from running the GitLab application.
1. [Monitoring infrastructure](/handbook/engineering/monitoring/): This is the infrastructure used to ensure GitLab.com is operating smoothly. System Logs are sent from GitLab.com to our monitoring infrastructure and collected by a FluentD collector. From FluentD, logs are either sent to long term Google Cloud Services cold storage via Stackdriver, or, they are sent to our Elastic Cluster via Cloud Pub/Sub which can be explored in real-time using Kibana.

### Self-managed

For Product Intelligence purposes, self-managed instances have two major components:

1. Data infrastructure: Having a data infrastructure setup is optional on self-managed instances. If you'd like to collect Snowplow tracking events for your self-managed instance, you can setup your own self-managed Snowplow collector and configure your Snowplow events to point to your own collector.
1. GitLab: A self-managed GitLab instance contains all of the same components as GitLab.com mentioned above.

### Differences between GitLab Inc and Self-managed

As shown by the orange lines, on GitLab.com Snowplow JS, Snowplow Ruby, Service Ping, and PostgreSQL database imports all flow into GitLab Inc's data infrastructure. However, on self-managed, only Service Ping flows into GitLab Inc's data infrastructure.

As shown by the green lines, on GitLab.com system logs flow into GitLab Inc's monitoring infrastructure. On self-managed, there are no logs sent to GitLab Inc's monitoring infrastructure.

Note (1): Snowplow JS and Snowplow Ruby are available on self-managed, however, the Snowplow Collector endpoint is set to a self-managed Snowplow Collector which GitLab Inc does not have access to.


## SaaS Data Collection Catalog

Our SaaS data collection catalog spans both the client (frontend) and server (backend), and uses various tools. We pick-up events and data produced when using the application. By utilizing collected [identifiers](/handbook/product/product-intelligence-guide/#data-used-as-identifiers), we can string these backend and frontend events together to illustrate a GitLab journey at the (1) [user](/handbook/product/product-intelligence-guide/#example-user-journey) (pseudonymized), (2) namespace, and project level.

The below table explains the types of data we collect from GitLab.com and examples for what it can be used. Below we show example data for each collection types.o

| Technology                                                                                                                            | Data Type          | Description                                                                                                                                                                                                                           | Aggregation Method                                    |
|---------------------------------------------------------------------------------------------------------------------------------------|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| [Snowplow Tracking](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/) (Frontend and Backend) | Event Based Data   | Examples:  - Collects an event on Git pushes - Collects an event on a button click - Collects an event on a successful Pipeline  - Collects an a request to a Rails controller                                                                                            | Event based or grouped by an attribute (e.g. session) |
| [Service Ping](https://docs.gitlab.com/ee/development/service_ping/)                                                                  | Transactional data | Examples: - Total issues created - Instance settings such as the instance's Git version - Integration settings such as an [external storage provider](https://docs.gitlab.com/ee/administration/static_objects_external_storage.html) | Count based on either total time our given timeframe  |


### Frontend Events
On the client side, a [Snowplow JS Tracker](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/javascript-trackers/) is used to track events.

The most up to date schema of the events can be found at https://gitlab.com/gitlab-org/iglu/.

#### Collected Data

By using the [Snowplow JS Tracker](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/javascript-trackers/) we're tracking user-event data like page views or user interactions like clicks.

Examples of what we can gather with this data:
- Number of events clicked on a button, modal, link
- Number of page views on a specific page

#### Frontend Event Example Data

_coming soon_

### Backend

We use [Snowplow Ruby Tracker](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/ruby-tracker/) to gather server-side event data. We also utilize [Service Ping](/handbook/product/product-intelligence-guide/#service-ping) to gather aggregated metrics from **PostgreSQL database** and a **Redis in-memory data store**. Lastly, the server also contains **System Logs** which are generated from running the GitLab application.

#### Backend Event Example Data

_coming soon_


##### Service Ping Example Data

A more detailed example of the service ping payload can be found in [our docs](https://docs.gitlab.com/ee/development/service_ping/#example-service-ping-payload)

```json
{
  "uuid": "0000000-0000-0000-0000-000000000000",
  "hostname": "example.com",
  "version": "12.10.0-pre",
  "installation_type": "omnibus-gitlab",
  "active_user_count": 999,
  "recorded_at": "2020-04-17T07:43:54.162+00:00",
  "edition": "EEU",
  "license_md5": "00000000000000000000000000000000",
  "license_id": null,
  "historical_max_users": 999,
  "licensee": {
    "Name": "ABC, Inc.",
    "Email": "email@example.com",
    "Company": "ABC, Inc."
  },
   "license_user_count": 999,
  "license_starts_at": "2020-01-01",
  "license_expires_at": "2021-01-01",
  "license_plan": "ultimate",
  "license_add_ons": {
  },
  "license_trial": false,
  "counts": {
    "assignee_lists": 999,
    "boards": 999,
    "ci_builds": 999,
    ...
  },
  "container_registry_enabled": true,
  "dependency_proxy_enabled": false,
  ...
}
```

### Data used as identifiers

In order to create the SaaS usage journeys documented below, we collect various identifiers in our data collection catalog. Where the identifier can be used to personally identify a user by someone without permissions to view that information, we will [pseudonymize](https://www.ucl.ac.uk/data-protection/guidance-staff-students-and-researchers/practical-data-protection-guidance-notices/anonymisation-and#Pseudonymisation) the data via [hashing at the collection layer](https://gitlab.com/groups/gitlab-org/-/epics/6309#hashing-on-the-collector-layer). Follow [this epic](https://gitlab.com/groups/gitlab-org/-/epics/6309) to track our progress on our pseudonymization project.

| |Metric | Example Data Pre-Pseudonymization|Pseudonymized? |Example Data Post-Pseudonymization |
|---|---|---|---|---|
|1|`user_ID`| `2890431` |Yes|Long hash|
|2|`project_ID`|`27005757`| No |`27005757`|
|3|`namespace_ID`|`12174719`|No|`12174719`|
|4|`page_url`|`https://gitlab.com/my-group/my-awesome-project`| Yes|`https://gitlab.com/group:123/project:356`  |
|5|`page_url`|`https://gitlab.com/my-group/my-awesome-project/some_folder/some_file.js` |Yes|`https://gitlab.com/group:123/project:346/:repository_path` 
|6|`page_url`|`https://gitlab.com/my-group/issues`| Yes|`https://gitlab.com/group:123/issues` |
|7|`page_url`|`https://gitlab.com/checkout`|Yes| `https://gitlab.com/checkout` | 
|8|`ip address`|`192.158.1.38`|No |`192.158.1.38`|


### Example User Journey

A user signups for a free GitLab.com account and creates a group and/or project (Pseudonymized user_id created and associated with group or project ID), they set up their repo and then view CI/CD and decide they want to invite a colleague to set up this functionality. The newly invited user signs up for GitLab (New pseudonymized user_id created and associated with the existing group or project ID) and sets up CI/CD for their team (backend event).

**Why this user journey is valuable to GitLab and our users.** In this example, by being able to connect pseudonymized user actions with backend actions we're are able to understand how often teams utilize this adoption path and at what rate they're successful. This will us know what work to prioritize to maximize improvements within the user experience and ensure we're able to understand how impactful these future iterations are.

### Which Tooling To Use

![Collection Framework Visual](collection_framework_fy21_q4-update.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

1. Plan-level reporting for Service Ping Redis on SaaS for multi-tenant reporting
1. Plan-level and Group-level reporting for Service Ping Postgres on SaaS for multi-tenant reporting
1. Service Ping Snowplow on Self-Managed (using internal collector and database for moderate volume)
1. Group-level, Project-level, User-level tracking for Snowplow on SaaS
1. Service Ping Snowplow on SaaS (using external collector and database for scaled up volume)

We use three methods to gather product usage data:

- [Snowplow](#snowplow)
- [Service Ping](#service-ping)
- [Database import](#database-import)

### Snowplow

Snowplow is an enterprise-grade marketing and product analytics platform which helps track the way
users engage with our website and application.

Snowplow consists of two components:

- [Snowplow JS](https://github.com/snowplow/snowplow/wiki/javascript-tracker) tracks client-side
events.
- [Snowplow Ruby](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/ruby-tracker/) tracks server-side events.

For more details, read the [Snowplow](https://docs.gitlab.com/ee/development/snowplow/index.html) guide.

### Service Ping

Service Ping is a method for GitLab Inc to collect usage data on a GitLab instance. Service Ping is primarily composed of row counts for different tables in the instance’s database. By comparing these counts month over month (or week over week), we can get a rough sense for how an instance is using the different features within the product. This high-level data is used to help our product, support, and sales teams.

With the exception of name and email of the license contact associated with customer instances, service ping does not collect personally identifiable (PII) data about the instances users. Service ping simply increments a number when a code path is executed in the application. It can function without any understanding of who users are.

For more details, read the [Service Ping](https://docs.gitlab.com/ee/development/service_ping/) guide.

### Database import

Database imports are full imports of data into GitLab's data warehouse. For GitLab.com, the PostgreSQL database is loaded into Snowflake data warehouse every 6 hours. For more details, see the [data team handbook](/handbook/business-ops/data-team/platform/#extract-and-load).

### Reporting level

Our reporting levels of aggregate or individual reporting varies by segment. For example, on Self-Managed Users, we can report at an aggregate user level using Service Ping but not on an Individual user level.

### Reporting time period

Our reporting time periods varies by segment. For example, on Self-Managed Users, we can report all time counts and 28 day counts in Service Ping.

## Metrics Dictionary

The Metrics Dictionary is a single source of truth for the metrics and events we collect for product usage data. The Metrics Dictionary lists all the metrics we collect in Service Ping, why we're tracking them, and where they are tracked.

The [Metrics Dictionary](https://docs.gitlab.com/ee/development/service_ping/metrics_dictionary.html) should be updated any time a new metric is implemented, updated, or removed. Currently, the metrics dictionary is built automatically once a day, so when a change to a metric is made in the .yml, you will see the change in the dictionary within 24 hours.

The Metrics Dictionary can be viewed for [Service Ping](https://docs.gitlab.com/ee/development/service_ping.html) and [Snowplow](https://docs.gitlab.com/ee/development/snowplow/index.html) data, however Snowplow metrics have not been fully converted to this method yet.  

### Metrics Dictionary features



![Metrics Dictionary Features](metric_dictionary_handbook.png)


1. **Filter data**. In the search bar, enter a value you want to filter the results set by.
1. **Customize viewable columns**. Select the options button to expand the "table fields" control. From here, you can select the columns you want to display in your view. Note, this doesn't not filter data by the selection, this only displays or not displays the data regardless of the values.
1. **Sisense query for GitLab.com**. Copy this query for use in Sisense. A common use case for this feature is to identify if data is available for SaaS Service Ping. Watch [this quick video](https://www.youtube.com/watch?v=n4o65ivta48) to learn more.
1. **Performance indicator type**. Metrics which are utiilized in business critical [xMAU](https://about.gitlab.com/handbook/business-technology/data-team/data-catalog/xmau-analysis/) calculations are indicated with a Performance indicator type value.
1. **Export**. You can now download the entire metrics dictionary as a .csv file.
1. **Metric Version**. Starting with miletone 13.9, we've begun to attribute the version associated with the metric. Unfortunately we couldn't populate the historical values for existing metrics so all prior metrics are labeled as `<13.9`. 
1. **Metric Product Section/Stage/Group**. You can display and/or filter by Section, Stage and Group as needed.
1. **Service Usage Data Category**. View and/or filter by [Service Usage Data](https://about.gitlab.com/handbook/legal/privacy/services-usage-data/) category (Optional, Operational, Subscription).

#### What’s next?

Take a look at our [current thinking about future iterations](https://gitlab.com/groups/gitlab-org/-/epics/6522#iteration-opportunities) and please let us know how we can improve this tool for you and your team!

## Implementing Product Performance Indicators

We've recently had a large push across the product organization to become more data driven. Part of this push includes getting product metrics in place for each product section, stage, and group. In FY21-Q3 OKRs, we setup a couple of OKRs to help us accomplish this:

- [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343).

To accomplish these OKRs, we setup a seven step process to implement product metrics. This process was originally presented in the Weekly Product Meeting on August 11, 2020 ([slide deck](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit) and [video presentation](https://youtu.be/1l1ru7k-q2I?t=375)) and has been refined over time.

| Implementation Status | Description | Responsibility | Exit Criteria |
| --------------------- | ----------- | -------------- | ------------- |
| [Definition](#definition) | The definition step outlines the process for deciding which product metrics to track. | PM Responsibility, Product Intelligence Support | Metric is defined in the Event Dictionary and [in the Performance Indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/performance_indicators) with the future `metric_name`. Issue for instrumentation is completed and scheduled for the current release. |
| [Instrumentation](#instrumentation) | The instrumentation step outlines how each product team implements data collection. | PM Responsibility, Product Intelligence Support | Instrumentation is completed and feature flags are turned off so that data can be collected |
| [Data Availability](#data-availability) | The data availability step outlines the timing of a product release to receiving product usage data in the data warehouse. | PM Responsibility, Product Intelligence Support | PM confirms that the `metric_name` is present in the data set, updates the PI file with a cc to `@gitlab-org/growth/product-intelligence/engineers @gitlab-data` to inform them that the metric is available. For example, for self-managed service ping implementation, check the #g_product_intelligence slack channel for the latest SaaS Service Ping Payload. |
| [Dashboard](#dashboard) | The dashboarding step outlines how Sisense dashboards are built. | PM Responsibility, Product Intelligence Support | There is a chart in Sisense. |
| [Handbook](#handbook) | The [Product PI handbook page](/handbook/product/performance-indicators/#how-to-work-with-pages-like-this) describes how product performance indicators are added for each product section, stage, and group. | PM Responsibility, Product Intelligence Support | Chart is embedded into the handbook. Target has been assigned based off the data. |
| [Target](#target) | The target definition step outlines how targets are defined for each performance indicator. | PM Responsibility, Product Intelligence Support | The target value is in both the chart and in the Performance Indicator (PI) file. |
| [Complete](#complete) | All of the prior steps have been completed. | PM Responsibility, Product Intelligence Support | :tada: |

### 1. Definition

Determine what metrics are important for your specific section, stage, or group.

**Instructions:**

1. Determine the level at which the metric should be measured:
    1. All product level - TMAU, Paid TMAU
    1. Stage level - SMAU, Paid SMAU, Other PI
    1. Group level - GMAU, Paid GMAU, Other PI
1. Define the metric. This is typically done by selecting an initial AMAU that is most representative of overall stage/group use
1. Determine the right [collection framework](/direction/product-intelligence-guide/#collection-framework) to utilize. Some guidance for considering collection frameworks:
    - **Service Ping - General** - There are many frameworks that report via Service Ping. All of them have various capabilities regarding types of events and available segementation. All Service Ping collection frameworks have a frequency of monthly for self-managed usage and every-other-week for SaaS usage.
    - **Snowplow** - Investment in instrumentation should be discouraged - there is already great URL and path tracking from SnowPlow for Gitlab.com. If additional tracking is needed on front-end events consider utilizing Service Ping Redis HLL. Snowplow instrumentation becomes immediately available on GitLab.com only.
    - **Service Ping - Redis HLL** - Redis HLL can track distinct counts, typically in front-end metrics on both the client (Javascript) and server (Ruby) side. Redis HLL does not provide session level segmentation on GitLab.com. As Service Ping instrumentation it is returned from both SaaS and Self-Managed.
    - **Service Ping - Postgres** - Our original Service Ping implementation this collects distinct database counts.
    - **Service Ping - Prometheus** - Note there is no future plan to increase the segmentation of this collection framework and it is presently limited to Instance wide metrics.
1. Plan your future events and metrics with your engineering team and add them to the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485).
1. Add the value from column A of the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) to the [Performance Indicator (PI) yaml file as the `metric_name`](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/performance_indicators)

#### Deduplication of Aggregated Metrics

Note: We now enable you to deduplicate aggregated metrics implemented via Redis HLL, in order to get distinct counts (ex distinct users count across multiple actions in a stage). Please read our docs on [Aggregated Metrics](https://docs.gitlab.com/ee/development/usage_ping/#aggregated-metrics) for more information. We are working towards the ability to deduplicate across multiple Database HLL metrics via [#288848](https://gitlab.com/gitlab-org/gitlab/-/issues/288848), and then deduplication across multiple Redis HLL and Database HLL metrics via [#421](https://gitlab.com/gitlab-org/product-intelligence/-/issues/421)

| Term | Definition | Example |
| ---- | ---------- | ------- |
| Aggregated | Metric contains rolled-up values due to an aggregate function (COUNT, SUM, etc) | Total Page Views (TPV) - the sum of all events when a page was viewed. |
| Deduplicated | Metric counts each unit of measurement once. | Unique Monthly Active Users (UMAU) - each user_id is counted once |
| Deduplicated Aggregated | Metric contains a rolled-up value where each unit is counted once. | UMAU is a deduplicated aggregated metric but TPV is not. |

### 2. Instrumentation

Work with your engineering team to instrument tracking for your XMAU. Focus on using Service Ping as your metrics will be available on both SaaS and self-managed.

The [Service Ping Guide](https://docs.gitlab.com/ee/development/service_ping/) outlines the steps required for instrumentation. It includes:

- [What is Service Ping?](https://docs.gitlab.com/ee/development/service_ping/#what-is-usage-ping)
- [How Service Ping works](https://docs.gitlab.com/ee/development/service_ping/#how-usage-ping-works)
- [Implementing Service Ping](https://docs.gitlab.com/ee/development/service_ping/#implementing-usage-ping)
- [Example Payload](https://docs.gitlab.com/ee/development/service_ping/#example-service-ping-payload)

Also see the [Product Intelligence Guide](/handbook/product/product-intelligence-guide/) and [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html).

**Instructions:**

1. Read the docs and work with your engineers on instrumentation.
1. Ask for a [Product Intelligence Review](https://docs.gitlab.com/ee/development/service_ping/#9-ask-for-a-product-intelligence-review) in your MR.

#### Metrics Instrumentation

We now have available [Metric Instrumentation Classes for Service Ping metrics](https://gitlab.com/groups/gitlab-org/-/epics/5547).

This means that we deprecate the usage of [`usage_data.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage_data.rb) and for adding a new metric we do not change this file. When you add or change a Service Ping Metric, you must migrate the metric to instrumentation classes. 

For metrics which are added dynamically, the Product Intelligence team is working to migrate these, see [epic](https://gitlab.com/groups/gitlab-org/-/epics/6140). In the case that a metric from this list is added/removed/changed before Product intelligence team has migrated the metric, the group owning the metric should perform the migration to instrumentation classes during the course of the desired change.

Adding a new metric in Service ping will require adding an instrumentation class and a metric definition, see more in [Metrics Instrumentation guide](https://docs.gitlab.com/ee/development/service_ping/metrics_instrumentation.html).

As we add more support to instrumentation classes, you can follow the progress in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/6118).

Note that not all the metrics in Service Ping are migrated to use instrumentation classes, this will be a continuous work and we kindly ask for cross-team collaboration to achieve this.

### 3. Data Availability

Plan instrumentation with sufficient lead time for data availability. Ensure your metrics make it into the self-managed release as early as possible.

**Timeline:**

1. [Self-managed releases](/upcoming-releases/) happen on the 22nd of each month (+30 days)
1. Wait one week for customers to upgrade to the new release and for a Service Ping to be generated (+7 days)
1. Service Pings are collected in the Versions application. The Versions application's database is automatically imported into the Snowflake Data Warehouse every day (+1 day).

In total, plan for up to 38 day cycle times (Examples [1](https://gitlab.com/gitlab-data/analytics/-/issues/5629#note_389671640), [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31785#note_392882428)). Cycle times are slow with monthly releases and weekly pings, so, implement your metrics early.

**Instructions:**

1. Merge your metrics into the next self-managed release as early as possible.
1. Wait for your metrics to be released onto production GitLab.com. These releases currently happen on a daily basis.
1. Service Pings are generated on GitLab.com on a weekly basis. Monitor the #g_product_intelligence slack channel where the Product Intelligence team will post the latest GitLab.com Service Ping ([example](https://gitlab.slack.com/files/ULXG09FAL/F01905UPPL0/12-gitlab.com-usage-data-for2020-08-04.json?origin_team=T02592416&origin_channel=CL3A7GFPF)). Verify your new metrics are present in the GitLab.com Service Ping payload.
1. Wait for the Versions database to be imported into the data warehouse.
1. Check the dbt model [version_usage_data_unpacked](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked#columns) to ensure the database column for your metric is present.
1. Check [Sisense](http://app.periscopedata.com/app/gitlab/) to ensure data is available in the data warehouse.

### 4. Dashboard

Dashboard the metric. This is done by creating a Sisense dashboard. Avoid cumulative views and instead focus on month-over-month growth. Instructions for creating dashboards are here.

We need PMs to self-serve their own dashboards as data team capacity is limited. The data team will be focused on enabling self-service, advising PMs, and working on the more challenging XMAU dashboards.

To learn how to create your own dashboard, see [Data For Product Managers: Creating Charts](/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)

To update the SMAU Summary Dashboards: [GitLab.com Postgres SMAU Dashboard (SaaS)](https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU) and [Service Ping SMAU Dashboard (SaaS + Self-Managed)](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard), please open a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues).

**Dashboard Prioritization**

For GMAU and SMAU data issues:

- In Q3, due to very limited data team capacity, the data team capacity will be reserved primarily for GMAU and SMAU.
    - Please add `XMAU` label to the data issues.
    - Please limit the ask to XMAU only, rather than all the supporting metrics.
- If there is still need to prioritize within GMAU issues, we will work with Scott and Section Leaders on the prioritization.
    - Section Leaders are encourage to [rank XMAU Issues](https://gitlab.com/gitlab-data/analytics/-/issues/5664#note_392529098) on the [Product Section Board](https://gitlab.com/gitlab-data/analytics/-/boards/1921369?label_name%5B%5D=Product).

For non-GMAU and non-SMAU data issues:

- In Q3, the data team will have very limited capacity to support non-GMAU and non-SMAU data requests.
    - Please continue to create data issues and label Data issues with the appropriate [Product Section Label](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=section).
- If there are critical or urgent data asks, please @hilaqu and your section leader in the issue, with an explanation of why. We will evaluate them on a case-by-case basis.

**Instructions:**

1. Read through [Data For Product Managers: Creating Charts](/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)
1. Self-serve your dashboard
1. If you need help, create a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues) and ask your Section Leader to prioritize.

### 5. Handbook

There are five Product PI pages: The [Product Team page](/handbook/product/performance-indicators/) and section pages for [Dev](h/handbook/product/dev-section-performance-indicators/), [Ops](/handbook/product/ops-section-performance-indicators/), [Sec](/handbook/product/sec-section-performance-indicators/), [Enablement](/handbook/product/enablement-section-performance-indicators/).

We need all PMs to ensure their PIs are showing on the performance indicator pages. Based off [What we're aiming for](#what-were-aiming-for)

To do so, we need a clear way to communicate to PMs exactly which PIs are remaining. We will be [adding placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906) into the [performance indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) so that all required PIs show in the handbook. Once a PI is implemented, the actual PI will replace the placeholder PI.For more information about how PIs and XMAUs are related to one another, see [PI Structure](/handbook/product/performance-indicators/#structure).

**Instructions:**

1. @jeromezng @kathleentam will [add placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906).
1. @jeromezng @kathleentam will then meet with each Section, Stage, and Group, to understand the implementation status of each PI and document any exceptions. Exceptions for PIs will then be signed off by David and the Section Leaders.
1. Keep your [performance indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) up to date as the implementation status changes.

### 6. Target

As a product organization, we need to get into the habit of understanding our baselines and setting targets for each stage & group. For the PI Target step, you will work with your Section or Group Leader to define targets for each of your XMAUs.

Set a growth target, and embed in the tracking dashboard. Growth targets should be ambitious but achievable.

**Instructions:**

1. Work with your Section or Group Leader to define a target.
1. Add the Target Line into your dashboard ([example](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8490496)).
1. If you need help, book a meeting with @jeromezng @kathleentam.

### 7. Complete

All of the prior steps have been completed and a PI is successfully implemented.

**Instructions**

1. Continually monitor your results, and refine your priorities based on results.
1. Line up your roadmap priorities to positively influence the adoption funnel, drive the performance metric, and hit the growth targets.
1. Deeply understand the drivers of your metric through customer interviews and other product usage data. Visualize this through a user adoption funnel that describes how users enjoy the value of the particular product area you are measuring.

## Our Commitment to Individual User Privacy in relation to Service Usage Data

While collecting service usage data is important to our business, GitLab equally values the privacy of its users. Learn more about our data pseudonymization process, commitment to our users, and how we utilize service usage data [here](/handbook/product/product-intelligence-guide/service-usage-data-commitment).


## Quick Links

| Resource | Description |
| -------- | ----------- |
| [Product Intelligence Guide](/handbook/product/product-intelligence-guide) | A guide to Product Intelligence |
| [Service Ping Guide](https://docs.gitlab.com/ee/development/service_ping/) | An implementation guide for Service Ping |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html) | An implementation guide for Snowplow |
| [Event Dictionary](/handbook/product/product-intelligence-guide#metrics-dictionary) | A SSoT for all collected metrics and events |
| [Privacy Policy](/privacy/) | Our privacy policy outlining what data we collect and how we handle it |
| [Implementing Product Performance Indicators](/handbook/product/product-intelligence-guide#implementing-product-performance-indicators) | The workflow for putting product performance indicators in place |
| [Product Intelligence Direction](/direction/product-intelligence/) | The roadmap for Product Intelligence at GitLab |
| [Product Intelligence Development Process](/handbook/engineering/development/growth/product-intelligence/) | The development process for the Product Intelligence groups |
| [FAQ](handbook/product/product-intelligence-guide/faq) | Product Intelligence FAQ |
