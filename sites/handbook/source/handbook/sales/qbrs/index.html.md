---
layout: handbook-page-toc
title: "GitLab Sales QBRs"
description: "Overview of GitLab's Quarterly Business Review process"
---

## Quarterly Business Review (QBR) Overview
QBRs are held during the **first two weeks of every quarter.** The sessions span between 5-7 business days. The goal of these sessions is to:
1. Review team performance and lessons learned from the past quarter
1. Review customer wins, use cases and insights to repeat "what good looks like" 
1. Review current quarter forecast and plans to meet and exceed sales goals in the coming quarter (and beyond)
1. Prepare CRO and GitLab E-Group for GitLab's quarterly earnings report
1. Identify and prioritize specific requests that will help Sales be successful

### QBR Schedules
QBRs are held in the following months and *typically* are a mix of in-person and virtual formats. This is subject to change based on any outside conditions that make it unsafe for the team to travel (i.e. COVID-19). 
1. Feb: Virtual
1. May: Virtual
1. Aug: In-person*
1. Nov: Virtual

*Subject to a shift to virtual based on [GitLab Travel Policy](/handbook/travel/#travel-guidance-covid-19)  

QBRs are typically organized by region and are held starting at the ASM/manager level and roll up by leadership tier from there. ASMs/managers are responsible for gathering the necessary insights and asks from their teams in self-organized, team-level meetings. See [QBR Best Practices](/handbook/sales/qbrs/#qbr-best-practices) section for more information. Schedule overlap is avoided to the best of the planning team's ability, but note that some overlap, especially cross-segment, is typically unavoidable. 

See below for a list of QBR sessions by segment. 

**ENT Sales QBRs**
1. ENT AMER Geo 
   1. East Central 
   1. Northeast 
   1. Southeast 
   1. West
   1. PacNW & Midwest, Rockies/SoCal
1. ENT PubSec Geo 
   1. National Security Group (NSG includes DHS, DoJ, and DoE)
   1. Civilian
   1. DoD 
   1. SLED
   1. FSI
1. ENT EMEA Geo 
   1. EMEA North
   1. EMEA South 
   1. DACH 
1. ENT APAC
1. ENT AMER Regional
   1. AMER East 
   1. AMER West 
1. ENT Leadership
   1. ENT AMER 
   1. ENT PubSec
   1. ENT EMEA
   1. ENT APAC 

**COMM Sales QBRs**
1. COMM SMB - AMER & EMEA
1. COMM MM - AMER & EMEA
1. COMM SMB & MM - APAC
1. COMM Sales Leadership (Closed Session)

**Channel & Alliances QBRs**
1. Channels & Alliances Program Leadership
   1. Alliances 
   1. Programs & Services 
   1. Channel Ops 
   1. Partner SA 
   1. Channel GTM
   1. Channel Marketing
1. Channel Geo/Field Leadership
   1. EMEA Channel
   1. PubSec Channel
   1. AMER Channel

**CS QBRs**
1. TAM Team - AMER & EMEA 
1. TAM Team - APAC 
1. SA Team
1. PubSec CS Team
1. Professional Services Team
1. CS Leadership 


![QBR Structure Pyramid](/handbook/sales/qbrs/QBR_Structure_Pyramid.png)


### QBR Participation
Sales QBRs include managers, leadership, and delegates from across the business: (for leadership QBR or CS QBR participation details, see below)

1. All Sales, Channel, and Customer Success ASMs or managers applicable for the QBR session
1. All Sales, Channel, and Customer Success leaders applicable for the QBR session (i.e. VP, AVP, RD)
1. 1-2 delegates from the following teams will participate 
   1. Sales Dev
   1. Field Marketing
   1. Product Marketing
   1. Demand Gen
   1. Product 
   1. Sales Ops
   1. Field Enablement
   1. Channel/Alliance
   1. People Team
1. Optional GTM leaders (* indicates optional on the QBR invite)
   1. CRO*
   1. All CRO Leadership*
   1. CMO*
   1. VP Revenue Marketing*
   1. VP Demand Gen*
   1. VP Product Management*
   1. Sr. Director of Sales Operations*
   1. Sr. Director Field Enablement*
   1. Sr. Director of Professional Services*
   1. Sr. Manager of Sales Operations* 
   1. Field Enablement Program Manager* (applicable for the QBR session, i.e. Sr. Program Manager, Commercial Sales Enablement)
   1. Field EBAs*
   1. Field Talent Acquisition Team*
   1. Customer Reference*
   1. Field Security*

**ENT Leadership QBR**
1. Segment Sales VP
1. Segment Sales VP direct reports (RDs, ASMs, and Inside Sales Managers)
1. Chief Revenue Officer (CRO) and CRO Chief of Staff (both optional)
1. Chief Marketing Officer (CMO) (optional)
1. CRO direct reports
1. VP of Demand Gen
1. VP of Revenue Marketing
1. VP of Product Management
1. VP of Field Ops direct reports
1. Sr. Program Manager, ENT Sales Enablement
1. Sr. Program Manager, Customer Success Enablement (optional)

**COMM Leadership QBR**
1. Segment Sales VP
1. Segment Sales VP direct reports (RDs, ASMs)
1. People Business Partner
1. Field Operations, COMM Support
1. Program Managers, COMM Sales Enablement

**CS Leadership QBR**
1. VP Customer Success
1. VP Customer Success direct reports
1. Chief Revenue Officer (CRO) and CRO EBA (both optional)
1. Chief Marketing Officer (CMO) (optional)
1. CRO direct reports
1. VP of Demand Gen
1. VP of Revenue Marketing
1. VP of Product Management
1. VP of Field Ops direct reports
1. People Business Partners
1. VP, Product Marketing
1. Sr. Program Manager, Customer Success Enablement


## QBR Best Practices

### Best Practices for Managers/ASMs/RDs/AVPs
1. Managers/ASMs are responsible for gathering insights and input from their teams to inform their QBR presentations. These sessions should be self-organized by the managers. It is recommended that managers look at these sessions as "family discussions" and keep them simple and informal to encourage team openness and knowledge-sharing. 
1. Depending on the QBR, RDs, AVPs or VPs are considered the "hosts" of their QBR sessions. As the hosts, these leaders serve as the DRI for the attendance and schedule of their sessions. Therefore, please ensure you are being thoughtful when providing the QBR planning team with your attendee list and schedule for your session.
1. QBRs should be an inclusive environment – QBR hosts should encourage participation from other members of their extended teams as well as delegates from other areas of the business.
   1. Invite your Channel Manager, SDR Manager, Field Marketing delegate, and/or TAM Manager to provide a brief (5 minute) update during each QBR.
   1. NOTE: ENT ASMs will co-present with SA managers. 
1. Any changes to the QBR schedules should be made no later than two weeks before the scheduled start of QBRs to avoid calendar disruptions for participants.
1. All managers/ASMs should complete their QBR decks no later than 2 business days prior to their QBR session. (Note: For QBRs that occur on the first business day of the new quarter, the standard is one business day.)
1. All AVPs/RDs/ASMs should pre-read other QBR decks for their session. Questions should be left ahead of time in the coordinating notes document.
   1. RDs/AVPs should review their team's QBR asks and pre-prepare a list of prioritized asks to review in their QBR session. (See the [Field Operations QBR Request Intake Process](/handbook/sales/qbrs/#intake) below for more information.)

### Best Practices for Attendees
1. Please join on time and ensure your microphone is muted to avoid disruptions.
1. Please be an active participant in the QBR session – join on video (if possible), answer questions related to your area of expertise/team, and participate in discussions.
1. Please contribute by taking notes during the QBRs. Everyone can (and should!) contribute, but delegates from these teams are expected to pitch in on note taking:
   1. Sales Operations
   1. Field Enablement
   1. Product Marketing
   1. Field Marketing
   1. Sales Dev
   1. Channel
   1. Alliances
1. Notes taken during QBRs should provide a detailed summary of the QBR and focus on key takeaways, value-add commentary and conversation. Since QBRs are not recorded, notes are the record of the QBR. See [Note Taking](/handbook/sales/qbrs/#qbr-note-taking) section below for full note taking guidance.

## QBR Communication
1. QBR-related Slack channels will be created no later than five weeks before the start of QBRs.
   1. The naming will follow this format:
      1. #qbr-comm-qxfyxx
      1. #qbr-ent-qxfyxx
      1. #qbr-channel-alliances-qxfyxx
      1. #qbr-cs-qxfyxx
   1. These channels will be used to:
      1. Coordinate asks/reminders for RD/ASM planning action items.
      1. Send reminders about QBR schedules and timing.
      1. Field questions about attendance.
      1. Field logistical questions (i.e. "what's the Zoom link?") that arise as QBRs are in process.
      1. Conduct follow-ups to QBR asks. (i.e. share how/where we have tracked QBR asks and inform the field of the process that will happen next to address them and where to look for updates)
   1. Uses do *not* include:
      1. Facilitate manager approval or feedback for QBR decks. This is meant to be handled by managers with their leaders via their own preferred channels.
      1. Make any QBR asks that should be included in QBR decks.
   1. These channels will be archived within 60 days of QBRs being completed.

### QBR Note Taking
The purpose of notes is to summarize and capture meaningful insights that come out of the *conversation* during a QBR - questions and answers, feedback, asks, etc. 

**Q: Who should take notes?**

A: If you are attending as a delegate, please note that it is part of your responsibility to contribute to note-taking in the QBR.

**Q: What specific information should the delegate capture?**

A: In order to serve as an active participant in QBRs, there is no need to take verbatim notes during the sessions. Providing a detailed summary of information that is shared is adequate. 
1. Make as detailed notes as possible *pertaining to key insights, decisions, asks, action items, or specific improvement requests*. Please include reference to the specific slide in the QBR presentation that pertains to the ask when possible. While the slides will not be shared with the broader team, this information will help key stakeholders easily find the referenced information.
1. Capture meaningful insights, points or key decisions that come from conversations during the QBR, not information already written in a slide.
### Where to Find Past QBRs
1. A full list of past QBR presentations can be found in the [QBR Google drive folder](https://drive.google.com/drive/u/0/folders/1QAWFYxT2-NzWpOUodepbl0O6zfsMxJGL). Note that only key QBR stakeholders are able to access the slides. To keep the information shared in QBRs SAFE, team members are unable to access past recordings.

## Field Operations QBR Ask/Request Process

### QBR Asks - Best Practices
1. Be very specific in your ask; general asks are not actionable. Asks should be geared towards helping customers & prospects; improving internal processes & productivity. 
   1. General topics of asks are: Product enhancement, Process, Pricing, Tooling, Etc. 
1. Business Impact: Explain how the “Ask” will impact the business. Will improve the customer engagement? Will it make the sales team more productive? Will it foster better collaboration amongst internal teams?
1. Owner (DRI): Assign the Directly Responsible Individual (DRI) for this ask. The DRI cannot be a Sales Mgr/Leader or Rep. 
1. Requested Due Date: Provide a due date for the request. This will assist with prioritization for the DRIs and internal teams working on the ask.
   1. Bonus: If there is an issue that has been created for your ask, link it in your deck!
1. The Ask should focus on cross-functional teams. Try to avoid “intra-sales” asks that can be addressed during the quarter as a standard course of business. 

### Intake

1. ASMs are encouraged to share their top asks for leadership and supporting teams as a part of their QBRs. The asks will be prioritized by segment  leadership before being passed off to Sales Ops.
   1. Enterprise Ask Prioritization Process
      1. ASM collects asks from teams, prioritizes and includes a slide of the top 3 asks in their QBR presentation
      1. RDs/AVPs will review and prioritize their region's top asks and present them in the ENT Leadership QBR.
      1. RDs and ENT Sales VP will review full list of ENT asks in subsequent RD meeting and will deliver a final list of prioritized ENT asks (no more than 5 asks per functional group) no later than the Monday following the ENT Leadership QBR.
   1. Commercial Ask Prioritization Process
      1. ASM collects asks from teams, prioritizes and includes a slide of the top 3 asks in their QBR presentation
      1. RDs will present their region's top asks in the COMM Leadership QBR.
      1. ASMs, RDs and COMM Sales VP will review full list of COMM asks and will deliver a final list of prioritized COMM asks no later than the Monday following the COMM Leadership QBR.


1. Field Operations will share a list of consolidated QBR asks and next steps for each no later than three weeks following the last QBR. 
1. The DRI for each ask is responsible for creating a new issue related to the QBRR ask OR identifying and updating an existing issue.  
   1. Leaders/Managers  are encouraged to discuss any asks that were not included in the final list with the requestor in their 1:1 to determine how/if any action should be taken.
   1. NOTE: Reps can/should make asks of Field Ops at any time outside of QBRs using [this template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/new?issuable_template=FieldOpsQBR).

### Execution and Tracking

All new QBR asks are tracked and managed as issues within the DRI's team Project and will include a global `QBR` label and year/quarter specific `FYxx-Qx QBR` label to ensure the asks are categorized appropriately for review.  


## Sample QBR Roll-Out
Below is a sample roll-out of the QBR planning and execution process that is followed each quarter. This process is subject to change based on sales leadership feedback and priorities. If you have questions, please reach out in #field-enablement-team.

#### 1 month before end of Q4 (each FY)
1. Field EBAs and Field Comms to collaborate and present a tentative schedule of QBRs for the following 4 quarters.
   1. Field Comms to socialize to segment VPs for buy-in
   1. Once approved, socialize to all AVPs and RDs for buy-in

#### 5 weeks before QBRs are scheduled to start
1. Field EBAs to finalize initial QBR Schedule, Attendee and Agenda document. (See past documents in the [Field Operatons Google Drive folder](https://drive.google.com/drive/u/0/folders/1Zy1AFhJoHo3_nXCiKU8gtM3a1UBBJu6Y).)
   1. Field Comms will provide a sample agenda for ENT and COMM segments to be used as a reference point for QBR hosts.
1. Field Comms to create a QBR issue that will be used to share:
   1. Agenda documents
   1. Deck templates
   1. Notes documents for each session
   1. A checklist of key dates and milestones
1. Field Comms will host a "QBR Kickoff Call" with the CRO, Segment VPs, and all Field Managers and above to overview the upcoming QBR planning process and reiterate asks/responsibilities. (All field managers will be added as optional.)
   1. If there are no material changes in the schedule or QBR process, this call can be canceled in favor of an async approach.
1. Field Comms to stand up QBR-specific Slack channels to address questions and requests from field managers.
1. Designated QBR hosts for each session (AVPs/RDs/ASMs or equivalent) to provide input in the following areas by EOW.
   1. Final approval/check on the assigned date for their QBR
   1. A full list of attendees – team members, relevant SA/TAM managers, any delegates they want to ensure are included (SDR managers, PMMs, etc.)
      1. Please note that RDs/ASMs are not required to select delegates from other teams for their sessions. This is a chance to request a specific team member (ex. a specific SDR manager). If an RD/ASM chooses not to, Field Enablement will work with managers from these teams to select delegates for each session.
   1. Logistical details (i.e. personal Zoom room link)

#### 4 weeks before QBRs are scheduled to start
1. Field EBAs to send out initial QBR calendar hold invites that include the following attendees:
   1. RDs, ASMs, and any other team members for that region - mandatory
   1. GTM leaders - optional (See [QBR Participation](/handbook/sales/qbrs/#qbr-participation) section above for full list)
1. Field Comms to engage delegate organizations to assign attendees from the following teams for each QBR in the QBR Schedule, Attendee and Agenda document by EOW.
   1. Sales Dev
   1. Field Marketing
   1. Product Marketing
   1. Demand Gen
   1. Product 
   1. Sales Ops
   1. Field Enablement
   1. Channel/Alliance
   1. People Team
   1. Inside Sales
1. Field Operations in coordination with Enablement Program Managers begin working with VPs on QBR deck templates (if applicable).

#### 3 weeks before QBRs are scheduled to start
1. Field Operations in coordination with Enablement Program Managers finalize QBR deck templates with VPs. QBR deck templates distributed to leaders for them to cascade down to their teams.
1. Sales Ops to create notes spreadsheet and notes documents for each QBR. Notes document to include the following:
   1. Note Taking Best Practices
   1. Example notes section
   1. Link to final QBR schedule
1. AVPs/RDs/ASMs to populate final QBR schedules in their respective QBR Schedule, Attendee and Agenda document/tab by EOW following the best-practices outlined in their respective Sample Schedule. 

#### 2 weeks before QBRs are scheduled to start
1. Further scheduling changes for QBR sessions are discouraged in order to avoid disruptions for attendees.
1. Field EBAs update existing QBR calendar invites with updated details: 
   1. Delegate assignments for each session
   1. Link to notes document
   1. Final schedule in spreadsheet tab

#### 1 week before QBRs are scheduled to start
1. ASMs/managers encouraged to complete their self-organized, team-level QBR sessions to gather final details for their formal QBR presentations. 
1. Field Comms to create a "Manager Responsibilies" issue outlining the asks/responsibilities for QBR hosts before, during and after the calls (i.e. reminder to record, where to upload recording, etc.) and socialize this issue in the QBR Slack channels. 
1. Field Comms to send a reminder to all QBR delegates regarding the [attendee best-practices](/handbook/sales/qbrs/#best-practices-for-attendees) outlined above in QBR Slack channels. 

#### Week(s) of QBRs
1. The AVP/RD or equivalent will lead the meeting and be responsible for recording the QBR.
1. Assigned note takers will take QBR notes following the [best practices](/handbook/sales/qbrs/#qbr-note-taking) outlined above.
1. Field EBAs should consistently monitor the QBR Slack channels for questions.

#### 1 week following QBRs
1. Field Comms to create a QBR feedback issue and socialize the link in each QBR Slack channel. Issue will remain open for two weeks after the final QBR session. 
1. QBR hosts should share a link to their QBR recording in the designated "QBR Recordings" tab of their segment's (ENT, COMM, Channel) schedule spreadsheet.
   1. Note: Where the recording lives is up to the host – Zoom link, YouTube link, Chorus link – as long as the link can be accessed by Field team members.
1. Field Ops team begins triaging and actioning on QBR asks. Execute on top-priority asks in a Cannonball run.

#### 3 weeks following QBRs
1. Field Ops completes initial review and assignment of consolidated/prioritized QBR asks in an issue. Assigns asks to the appropriate DRIs. Determines which Ops-related asks will be actioned on in the quarterly Cannonball run. 
1. Field Ops collaborates with Field Comms to socialize the consolidated list of QBR asks and next steps. 


