---
layout: job_family_page
title: Corporate Communications Manager
---

## Corporate Communications Manager

Corporate Communications managers at GitLab have a unique opportunity to contribute to our [mission](/company/mission/#mission). Through your leadership, experience, and insight, you will position GitLab as a trustworthy partner to customers with a strong track record of addressing business challenges, increase overall brand awareness, provide air cover for business as a whole, and come up with thought leadership opportunities. This position will work across teams and the globe to develop and execute integrated communications campaigns in line with GitLab initiatives. You will be expected to prioritize and manage your work with minimal guidance from leadership or other corporate communications team members.

We work in a unique way at GitLab, where flexibility and independence meet a high paced, pragmatic way of working. And everything we do is in the open. We recommend looking at our [corporate marketing handbook](/handbook/marketing/corporate-marketing/) to get started.

## Job Grade

The Corporate Communications Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Execute integrated communications strategy and vision.
* Work closely with others in the Corporate Communications department to develop messaging, press releases, contributed articles, event strategy and media relations strategy for GitLab announcements and corporate marketing campaigns.
* Respond to daily media inquiries in a timely and professional manner.
* Work across teams to find out potential news or story ideas.
* Report back on press activities, coverage, opportunities, successes and press feedback.
* Measure our department successes in relation to awareness and impact.
* Independently manage projects from start to finish.

## Requirements
* 6+ years experience in public relations and/or marketing communications.
* PR agency experience.
* Experience in enterprise software or developer public relations.
* Proven experience in working with agencies and freelancers.
* Strong media relations skills.
* A natural storyteller with excellent writing skills.
* Able to coordinate across many teams and perform in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills.
* You share our values, and work in accordance with those values.
* Willingness to work within GitLab.com.

### Senior Corporate Communications Manager

#### Job Grade

The Senior Corporate Communications Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Develop integrated communications campaigns.
* Develop overarching company storylines for media.
* Have your finger on the pulse of the news and provide creative ideas to gain media coverage.
* Manage GitLab’s PR agency relationships and ensure their programs are in line with overall corporate marketing objectives and goals.
* Conduct media training for spokespeople as needed.
* Strategically lead crisis communications efforts as needed.
* Responsible for ideation of Corp Communications activities, OKRs, and reporting on results.

#### Requirements
* 8+ years experience in public relations and/or marketing communications
* Experience with crisis communications, executive visibility and investor relations.
* Experience managing agencies and driving results.
* A natural storyteller with excellent writing skills.
* Highly organized, detail-oriented and able to drive cross-functional deadlines consistently.

### Manager, Corporate Communications

#### Job Grade
Manager, Corporate Communications is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Develop QoQ and YoY integrated cross-functional communications programs.
* Develop overarching company storylines for media.
* Manage a team of direct reports, freelancers and/or agencies to scale integrated communications programs and ensure their results contribute to the  corporate communications strategy.
* Manage media training schedule and ensure 100% compliance of training completion.
* Strategically lead crisis communications efforts as needed.
* Develop program’s OKRs and deliver quarterly reporting of results.
* Responsible for ideation of Corp Communications activities, OKRs, and reporting on results.

#### Requirements
* 9+ years experience in public relations and/or marketing communications.
* Experience developing and executing cross-functional communications programs.
* Experience managing agencies, freelancers and a team of direct reports to drive results.
* A natural storyteller with excellent writing skills.
* Outstanding verbal communications skills.
* Highly organized, detail-oriented and able to drive cross-functional deadlines consistently.

### Staff Corporate Communications Manager

#### Job Grade
Staff Corporate Communications Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

**Market Justification:** The business need for an individual contributor Corporate Communications role is to have a team member with a high level of expertise in Corporate, Internal or Executive Communications to be able to develop programs and run each specialized function. Additionally, this role is specialized due to industry experience required to collaborate with investors, e-group members, board members and other senior stakeholders. Market data yields an output of 35 companies that use this type of level with an average of 2 incumbents. 

## Responsibilities
* Develop QoQ and YoY integrated communications programs.
* Develop overarching company storylines for media.
* Strategically lead crisis communications efforts as needed.
* Develop program’s OKRs and deliver quarterly reporting of results.
* Responsible for ideation of Corp Communications activities, OKRs, and reporting on results.

#### Requirements
* 9+ years experience in public relations and/or marketing communications
* Experience developing and executing communications programs.
* Experience driving results.
* A natural storyteller with excellent writing skills.
* Outstanding verbal communications skills.
* Highly organized, detail-oriented and able to drive cross-functional deadlines consistently.

### Senior Manager, Corporate Communications

#### Job Grade
Senior Manager, Corporate Communications is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Managing a portion of the Corporate Communications budget. 
* Develop and execute integrated programs that further the overall corporate marketing objectives and goals.
* Manage a team of direct reports, freelancers and agencies, ensuring their results contribute to the overall corporate marketing objectives and goals.

#### Requirements
* 10+ years experience in public relations and/or marketing communications.
* Experience managing budgets aligning with communications objectives and goals.
* A natural storyteller with excellent writing skills.
* Outstanding verbal communications skills.
* Experience managing a team whose results drove the overall communications strategy.

### Principal Corporate Communications Manager

#### Job Grade

The Principal Corporate Communications Manager is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

**Market Justification:** The business need for an individual contributor Corporate Communications role is to have a team member with a high level of expertise in Corporate, Internal or Executive Communications to be able to develop programs and run each business function. Additionally, this role is specialized due to industry experience required to collaborate with investors, e-group members, board members and other senior stakeholders. Survey data yields a return of 18 organizations with an average of 1 incumbent in the principal level within Marketing Communications. A comparable role at GitLab is the [Principal Internal Communications Manager](https://about.gitlab.com/job-families/marketing/internal-communications/#principal-internal-communications-manager). 

## Responsibilities
* Lead public company communication program; including quarterly earnings press releases and activities, investor updates, media interviews, etc.
* Lead business storyline portion of overall Corporate Communications strategy; including developing press releases and contributed articles, event strategy and media relations strategy for GitLab announcements and news.
* Work closely with executives communications lead to align executive visibility program; including but not limited to key message creation, media relationship building, media tours, contributed articles, and speaking presence.

#### Requirements
* 10+ years experience in public relations and/or marketing communications.
* Experience running public relations/communications efforts at an enterprise technology company.
* A natural storyteller with excellent writing skills.
* Experience leading public company communications cadence (earnings reports, media outreach, etc.)
* Experience hiring and managing a team of public relations professionals.

### Director, Corporate Communications
The Director of Corporate Communications leads the overall corporate communications team, including public relations, executive communication, internal communication and social media work.

### Job Grade
The Director of Corporate Communications is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

### Requirements
* 12+ years experience managing public relations and communications teams
* Experience developing overall strategy for corporate communications, public relations and social media teams.
* Experience managing budgets aligning with communications objectives and goals.
* A natural storyteller with excellent writing skills.
* Outstanding verbal communications skills.
* Experience working with leadership to drive external awareness activities.

## Specialties

### Executive Communications
The executive communications manager will work closely with the executive leadership team to develop content for executive speaking opportunities, extend the reach of executive participation in external events, develop a platform for “Pick Your Brain” or other Zoom series, executive contributed articles and more.

#### Responsibilities
* Own the executive visibility program, driving its strategy and executing on the strategy.
* Work closely with the executive leadership team to develop strong narrative presentations and content for external speaking opportunities.
* Work with the brand, design, technical marketing team and others needed to develop state of the art slide decks for external events.
* Lead speaking coaching sessions with the leadership team to ensure they comfortably deliver the content in a natural and charismatic way; incorporate edits as needed.
* Think strategically about other executive leadership platforms (aspirational and industry peers) and how we should be promoting them externally, whether that is via our blog, contributed articles, podcasts, YouTube, social media or other external platforms.
* Work closely with others in the Corporate Communications department and with PR agencies to share presentation narratives for media consumption and obtain media messages/interview feedback to incorporate in external speaking presentations.
* Manage any outside agencies or freelancers hired to assist with the executive visibility program.
* Work closely with Executive Business Administrators to plan executive speaking schedules for the year.

#### Requirements
* Experience in public relations and/or marketing communications.
* Experience developing and executing executive communications programs.
* Experience in enterprise software and/or open source.
* Creative, thoughtful and passionate about storytelling.
* Outstanding written and verbal communications skills.
* Experience with presentation building - both narrative flow and design.
* Able to coordinate across many teams and perform in a fast-moving environment.
* Proven ability to be self-directed and work with minimal supervision.
* You share our values, and work in accordance with those values.
* Highly organized, detail-oriented and able to meet deadlines consistently.

### PR
The GitLab [PR Manager](/job-families/marketing/pr-manager/) will help GitLab build thought leadership and drive conversation in external communications channels. This position is responsible for developing and driving thought leadership content, securing media coverage for product, partner, channel, community and company announcements and stories, assisting with crisis and rapid response requests, implementing proactive PR campaigns and company award submissions.

### Social Marketing
The [Social Marketing Manager](/job-families/marketing/social-marketing-manager/) is a key part of building GitLab’s brand into one that is loved by its audience of developers, IT ops practitioners, and IT leaders.

### Internal Communications
The [Principal Internal Communications Manager](https://about.gitlab.com/job-families/marketing/internal-communications/) is part of the Talent Brand team within the PeopleOps group, but works very closely with the Corporate Communications team. Internal Communications plans and improves company-wide team member communications to ensure team members feel informed and heard.

## Career Ladder

The next step in the Corporate Communications job family is not yet defined at GitLab. 

## Performance Indicators
* Increase overall brand awareness through key message pull through in media articles and speaking presentations, secure opportunities for executives to speak at conferences and with media, submit strong award applications.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a screening call with one of our Global Recruiters.
* Candidates then may be offered a 30 minute interview with our Senior Manager, Corporate Communications.
* Next, candidates will be invited to schedule an interview with the Senior Director, Corporate Marketing.
* Candidates will then be invited to schedule interviews with key team members from relevant company departments that the role will work with closely.
* Next, candidates will be invited to schedule an interview with the Chief Marketing Officer.
* For Executive roles, candidates may be invited to put together a presentation for our Executive team.
* Finally, our CEO and other executives may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via video, phone, or email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
